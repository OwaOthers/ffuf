<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    
    public function register()
    {

        $this->app->bind(
            'App\Repositories\Interfaces\UserRepositoryInterface',
            'App\Repositories\Eloquent\UserRepositoryEloquent'
        ); 

        $this->app->bind(
            'App\Repositories\Interfaces\GenreRepositoryInterface',
            'App\Repositories\Eloquent\GenreRepositoryEloquent'
        );  
        
        $this->app->bind(
            'App\Repositories\Interfaces\SectionRepositoryInterface',
            'App\Repositories\Eloquent\SectionRepositoryEloquent'
        ); 

        $this->app->bind(
            'App\Repositories\Interfaces\BookRepositoryInterface',
            'App\Repositories\Eloquent\BookRepositoryEloquent'
        );         

    }


}
    