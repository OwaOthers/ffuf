<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    protected $fillable = [

    	'title',
    	'author',
    	'genre_id',
    	'section_id',
    	'status',
        'borrowed_by'
    ];

    public function genre(){

    	return $this->hasOne('App\Genre','id','genre_id');
    }

    public function section(){

    	return $this->hasOne('App\Section','id','section_id');
    }
}
