<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\BookRepositoryInterface;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $request;    
    protected $book;
    
    public function __construct(Request $request,        
        BookRepositoryInterface $book){

        $this->request = $request;        
        $this->book = $book;

    }

    public function index(){

    	$borrowed = count($this->book->onUser());

    	$available = count($this->book->onLibrary());

    	$books_cnt = count($this->book->all());

    	return view('dashboard.index',compact('borrowed','available','books_cnt'));
    }
}
