<?php

namespace App\Http\Controllers;

use App\Book;
use App\Repositories\Interfaces\GenreRepositoryInterface;
use App\Repositories\Interfaces\SectionRepositoryInterface;
use App\Repositories\Interfaces\BookRepositoryInterface;
use App\Http\Requests\BookRequest;
use Illuminate\Http\Request;

class BookController extends Controller
{
    
    protected $request;
    protected $genre;
    protected $section;
    protected $book;
    
    public function __construct(Request $request,
        GenreRepositoryInterface $genre,
        SectionRepositoryInterface $section,
        BookRepositoryInterface $book){

        $this->request = $request;
        $this->genre = $genre;
        $this->section = $section;
        $this->book = $book;

    }

    public function index()
    {
        $books = $this->book->all();

        $filters = [

            0 => 'All',
            1 => 'Author',
            2 => 'Section',
            3 => 'Title'
        ];
        $selected_filter = [];
        
        return view('book.index',compact('books','filters','selected_filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = $this->genre->getList();
        $sections = $this->section->getList();

        return view('book.create',compact('genres','sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $this->book->Create($this->request->all());

        return redirect('books')->with('is_success','Saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $genres = $this->genre->getList();
        $sections = $this->section->getList();

        return view('book.edit',compact('genres','sections','book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {
        $this->book->Update($this->request->all(),$id);

        return redirect('books')->with('is_update','update');
    }

    
    public function borrow(){
        
        $books = $this->book->onLibrary();

        return view('book.borrow',compact('books'));
    }

    public function return_book(){

        $books = $this->book->onUser();

        return view('book.return',compact('books'));   
    }

    public function borrow_store(Request $request){

        $books = $this->request->get('book_id');

        $type = $this->request->get('type');

        if(!empty($books))
        {
            if($type < 1){

                $borrower = $this->request->get('name');
                
                foreach($books as $book){

                    $postdata = [];
                    $postdata['status'] = 1;
                    $postdata['borrowed_by'] = $borrower;

                    $this->book->Update($postdata,$book);
                }            
            }else{
            
                foreach($books as $book){

                    $postdata = [];
                    $postdata['status'] = 0;
                    $postdata['borrowed_by'] = "";

                    $this->book->Update($postdata,$book);
                }            
            }
            return back()->with('is_success','Saved');
        }else{

            return back()->with('is_error','Error');
        }        
    }
}
