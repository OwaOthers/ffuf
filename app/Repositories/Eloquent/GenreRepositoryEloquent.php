<?php namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\GenreRepositoryInterface;
use App\Genre;

class GenreRepositoryEloquent implements GenreRepositoryInterface{

	protected $genre;

	public function __construct(Genre $genre)
	{

		$this->genre = $genre;
	}

	public function All(){

		return $this->genre->all();
	}

	public function ById($id){

		return $this->genre->find($id);
	}

	public function Create($attributes){

		$this->genre->fill($attributes)->save();

		return $this->genre->id;
	}

	public function Update($attributes, $id){

		if($id){

			$this->ById($id)->fill($attributes)->save();

			return $id;
		}
	}		

	public function getList(){

		return $this->genre->pluck('genre','id');
	}
}