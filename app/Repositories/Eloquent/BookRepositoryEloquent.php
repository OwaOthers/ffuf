<?php namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\BookRepositoryInterface;
use App\Book;

class BookRepositoryEloquent implements BookRepositoryInterface{

	protected $book;

	public function __construct(Book $book)
	{

		$this->book = $book;
	}

	public function All(){

		return $this->book->all();
	}

	public function ById($id){

		return $this->book->find($id);
	}

	public function Create($attributes){

		$this->book->fill($attributes)->save();

		return $this->book->id;
	}

	public function Update($attributes, $id){

		if($id){

			$this->ById($id)->fill($attributes)->save();

			return $id;
		}
	}		

	public function onLibrary(){

		return $this->book->where('status',0)->get();
	}

	public function onUser(){
		return $this->book->where('status',1)->get();	
	}
}