<?php namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\SectionRepositoryInterface;
use App\Section;

class SectionRepositoryEloquent implements SectionRepositoryInterface{

	protected $section;

	public function __construct(Section $section)
	{

		$this->section = $section;
	}

	public function All(){

		return $this->section->all();
	}

	public function ById($id){

		return $this->section->find($id);
	}

	public function Create($attributes){

		$this->section->fill($attributes)->save();

		return $this->section->id;
	}

	public function Update($attributes, $id){

		if($id){

			$this->ById($id)->fill($attributes)->save();

			return $id;
		}
	}		

	public function getList(){

		return $this->section->pluck('section','id');
	}
}