<!DOCTYPE html>
<html lang="en">  
<head>
	    <meta charset="utf-8">
	    <title>RIO'S LIBRARY SYSTEM</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <meta name="csrf-token" content="{{ csrf_token() }}">

	    <!-- Bootstrap core CSS -->
	    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		
		<!-- Font Awesome -->
		<link href="/assets/css/font-awesome.min.css" rel="stylesheet">

		<!-- ionicons -->
		<link href="/assets/css/ionicons.min.css" rel="stylesheet">
		
		<!-- Morris -->
		<link href="/assets/css/morris.css" rel="stylesheet"/>	

		<!-- datatable -->
		<link href="/assets/css/dataTables.bootstrap.css" rel="stylesheet"

		<!-- Datepicker -->
		<link href="/assets/css/datetimepicker.css" rel="stylesheet"/>	

		<!-- Animate -->
		<link href="/assets/css/animate.min.css" rel="stylesheet">

		<!-- Owl Carousel -->
		<link href="/assets/css/owl.carousel.min.css" rel="stylesheet">
		<link href="/assets/css/owl.theme.default.min.css" rel="stylesheet">

		<link href="/assets/bootstrap-multiselect-0.9.13/css/bootstrap-multiselect.css" rel="stylesheet">

		<!-- Joshua Fradejas -->
		<link href="/assets/css/simplify.css" rel="stylesheet">
	
  	</head>

  	<body class="overflow-hidden">
		<div class="wrapper preload">			
			<header class="top-nav">
				<div class="top-nav-inner">
					<div class="nav-header">
						<button type="button" class="navbar-toggle pull-left sidebar-toggle" id="sidebarToggleSM">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>																	
						<a href="/" class="brand">
							<i class="fa fa-database"></i><span class="brand-name">RIO'S LIBRARY SYSTEM</span>
						</a>
					</div>
					<div class="nav-container">
						<button type="button" class="navbar-toggle pull-left sidebar-toggle" id="sidebarToggleLG">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>							
						</button>												
						<div class="pull-right m-right-sm">
							<div class="user-block hidden-xs">
								<a href="#" id="userToggle" data-toggle="dropdown">									
									<div class="user-detail inline-block">
										{!! Html::decode(link_to_route('logout', '<i class="fa fa-power-off fa-lgt"></i><span class="m-left-xs">Sign out </span>',array(), ['class' => 'large button'])) !!}												
									</div>
								</a>								
							</div>							
						</div>
					</div>							
				</div><!-- ./top-nav-inner -->	
			</header>
			<aside class="sidebar-menu fixed">
				<div class="sidebar-inner scrollable-sidebar">
					<div class="main-menu">
						<ul class="accordion">
												
							<li class="menu-header">
								Main Menu
							</li>							
							<li class="bg-palette1">
								<a href="/dashboard">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-dashboard fa-lg"></i></span>
										<span class="text m-left-sm">Dashboard</span>
									</span>
									<span class="menu-content-hover block">
										Dashboard
									</span>
								</a>
							</li>						
																			
							<li class="openable bg-palette3">
								<a href="#">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-gears fa-lg"></i></span>
										<span class="text m-left-sm">Settings</span>
										<span class="submenu-icon"></span>
									</span>
									<span class="menu-content-hover block">
										Settings
									</span>
								</a>
								<ul class="submenu bg-palette4">
									<li><a href="/genres"><span class="submenu-label">Genres</span></a></li>						
									<li><a href="/sections"><span class="submenu-label">Sections</span></a></li>								
								</ul>
							</li>												
							<li class="bg-palette4">								
								<a href="/books">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-book fa-lg"></i></span>
										<span class="text m-left-sm">Books</span>
									</span>
									<span class="menu-content-hover block">
										Books
									</span>
								</a>
							</li>				
							<li class="bg-palette2">								
								<a href="/borrow_books">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-file fa-lg"></i></span>
										<span class="text m-left-sm">Borrow Book</span>
									</span>
									<span class="menu-content-hover block">
										Borrow Books
									</span>
								</a>
							</li>			
							<li class="bg-palette1">								
								<a href="/return_books">
									<span class="menu-content block">
										<span class="menu-icon"><i class="block fa fa-exchange fa-lg"></i></span>
										<span class="text m-left-sm">Return Books</span>
									</span>
									<span class="menu-content-hover block">
										Return Books
									</span>
								</a>
							</li>								
						</ul>
					</div>						
				</div><!-- sidebar-inner -->
			</aside>			
			<div class="main-container">
				<div class="padding-md">
					
					@section('main-body')                    
                    @show									
					
				</div><!-- ./padding-md -->
			</div><!-- /main-container -->			
		</div><!-- /wrapper -->

		<a href="#" class="scroll-to-top hidden-print"><i class="fa fa-chevron-up fa-lg"></i></a>		
	
		<!-- Jquery -->
		<script src="/assets/js/jquery-1.11.1.min.js"></script>
		
		<!-- Bootstrap -->
	    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
	  
		<!-- Flot -->
		<script src='/assets/js/jquery.flot.min.js'></script>

		<!-- Datatable -->
		<script src='/assets/js/jquery.dataTables.min.js'></script>
		<script src='/assets/js/uncompressed/dataTables.bootstrap.js'></script>

		<!-- Slimscroll -->
		<script src='/assets/js/jquery.slimscroll.min.js'></script>
		
		<!-- Morris -->
		<script src='/assets/js/rapheal.min.js'></script>	
		<script src='/assets/js/morris.min.js'></script>	

			<!-- Moment -->
		<script src='/assets/js/uncompressed/moment.js'></script>

		<!-- Datepicker -->
		<script src='/assets/js/uncompressed/bootstrap-datetimepicker.js'></script>

		<!-- Sparkline -->
		<script src='/assets/js/sparkline.min.js'></script>

		<!-- Skycons -->
		<script src='/assets/js/uncompressed/skycons.js'></script>
		
		<!-- Popup Overlay -->
		<script src='/assets/js/jquery.popupoverlay.min.js'></script>

		<!-- Easy Pie Chart -->
		<script src='/assets/js/jquery.easypiechart.min.js'></script>

		<!-- Sortable -->
		<script src='/assets/js/uncompressed/jquery.sortable.js'></script>

		<!-- Owl Carousel -->
		<script src='/assets/js/owl.carousel.min.js'></script>

		<!-- Modernizr -->
		<script src='/assets/js/modernizr.min.js'></script>
		
		<!-- Simplify -->
		<script src="/assets/js/simplify/simplify.js"></script>
		<!-- Date Time picker -->
		<script src='/assets/js/uncompressed/bootstrap-datetimepicker.js'></script>

		<!-- Multiselect -->
    	<script src="/assets/bootstrap-multiselect-0.9.13/js/bootstrap-multiselect.js"></script>
			
		<script type="text/javascript">
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
      	</script>
      	<script type="text/javascript">    
	        $(document).ready(function() {
	                    
	            @section('page-script')
	            
	            @show	          
	        });
    	</script>    
    	<script>
			$(function()	{
			    $('#dataTable').dataTable();
			});
		</script>
  	</body>
</html>
<!-- Software Developer: JOSHUA M. FRADEJAS -->