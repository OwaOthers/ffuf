@extends('layouts.master')
@section('main-body')
<h3 class="header-text m-top-lg">Section Maintenance</h3>
<div class="row">
	<div class="col-lg-12 animated flash">
	    <?php if (session('is_success')): ?>
	        <div class="alert alert-success alert-dismissible fade in" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	        </button>
	            <center><h4 style="color:white">Section was successfully added!<i class="fa fa-check"></i></h4></center>                
	        </div>
	    <?php endif;?>
	    <?php if (session('is_update')): ?>
	        <div class="alert alert-success alert-dismissible fade in" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	        </button>
	            <center><h4 style="color:white">Section was successfully updated!<i class="fa fa-check"></i></h4></center>                
	        </div>
	    <?php endif;?>
	</div>
</div>
<div class="smart-widget widget-green">
	<div class="smart-widget-header">
		Sections Table		
		{!! Html::decode(link_to_Route('sections.create', '<i class="fa fa-plus"></i> New Section', [], ['class' => 'btn btn-default btn-xs pull-right'])) !!}	    	
	</div>	
	<div class="smart-widget-inner">		
		<div class="smart-widget-body">
			<table class="table table-hover">
	      		<thead>
	        		<tr>	          	
	        			<th>#</th>
	        			<th>Section</th>		          				
	          			<th>Action</th>
	        		</tr>
	      		</thead>
	      		<tbody>
					@foreach($sections as $section)									    	        	
		        	<tr>		          		      		        			           			       				          		         
		          		<td>{!! $section->id !!}</td>		          				          		          		   	
		          		<td>{!! $section->section !!}</td>			          		
		          		<td>{!! Html::decode(link_to_Route('sections.edit','<i class="fa fa-pencil"></i> Edit', $section->id, array('class' => 'btn btn-info btn-xs')))!!}</td>
		        	</tr>
		        	@endforeach		        	
	      		</tbody>
	    	</table>
		</div>
	</div><!-- ./smart-widget-inner -->
</div><!-- ./smart-widget -->
@stop