@extends('layouts.master')
@section('main-body')
<h3 class="header-text m-top-lg">Modifying Genre</h3>
<div class="row">
	<div class="col-md-12 animated flash">
	    @if(count($errors) > 0)
	        <div class="alert alert-danger alert-dismissible flash" role="alert">            
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	    </button>
	            <center><h4>Oh snap! You got an error!</h4></center>   
	        </div>
	    @endif
	</div>    
</div>
<div class="col-lg-6">
	<div class="smart-widget widget-green">	
		<div class="smart-widget-header">
			Genre Form							
		</div>
		<div class="smart-widget-inner">		
			<div class="smart-widget-body">
				{!!Form::open(array('route'=>array('genres.update',$genre->id),'method'=>'PUT','files'=>true))!!}					
					<div class="form-group">
						{!! Form::label('genre','Genre') !!}
						{!! Form::text('genre',$genre->genre,['class'=>'form-control','placeholder'=>'Enter Genre','required'=>'required']) !!}
						@if($errors->has('genre')) <p class="help-block" style="color:red;">{{ $errors->first('genre') }}</p> @endif      
					</div>						
					<div class="form-group">
						{!! Html::decode(link_to_Route('genres.index', '<i class="fa fa-arrow-left"></i> Cancel', [], ['class' => 'btn btn-default'])) !!}
	                    {!! Form::button('<i class="fa fa-save"></i> Update', array('type' => 'submit', 'class' => 'btn btn-success')) !!}
					</div>	
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop