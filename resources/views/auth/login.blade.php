
<!DOCTYPE html>
<html lang="en">  
<head>
	    <meta charset="utf-8">
	    <title>RIO'S LIBRARY SYSTEM</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <meta name="csrf-token" content="{{ csrf_token() }}">

	    <!-- Bootstrap core CSS -->
	    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		
		<!-- Font Awesome -->
		<link href="/assets/css/font-awesome.min.css" rel="stylesheet">

		<!-- ionicons -->
		<link href="/assets/css/ionicons.min.css" rel="stylesheet">
			
		<link href="/assets/css/simplify.min.css" rel="stylesheet">
	
  	</head>

  	<body class="overflow-hidden light-background">
		<div class="wrapper no-navigation preload">
			<div class="sign-in-wrapper">
				<div class="sign-in-inner">
					<div class="login-brand text-center">						
						<center><strong class="text-skin">RIO'S </strong><strong style="color:#455A64;">LIBRARY SYSTEM</strong></center>
					</div>
					<form role="form" method="POST" action="/login">
						<?php if (Session::get('errorMessage')):?>
		                    <div class="alert alert-danger text-center alert-dismissible flash" role="alert">
		                        <center><h4> {!! Session::get('errorMessage') !!}</h4></center>
		                    </div>                                                                        
		                <?php endif;?>
		                @csrf
		                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<div class="form-group m-bottom-md">
							<label>Name </label>
							<input type="text" class="form-control" placeholder="Name" name="name" required="">
						</div>
						<div class="form-group">
							<label>Password </label>
							<input type="password" class="form-control" placeholder="Password" name="password" required="">
						</div>						
						<div class="m-top-md p-top-sm">
							<button type="submit" class="btn btn-success block form-control">Sign in</button>				
						</div>						
					</form>
				</div><!-- ./sign-in-inner -->
			</div><!-- ./sign-in-wrapper -->
		</div><!-- /wrapper -->

		<a href="#" id="scroll-to-top" class="hidden-print"><i class="icon-chevron-up"></i></a>

	    <!-- Le javascript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
		
		<!-- Jquery -->
		<script src="/assets/js/jquery-1.11.1.min.js"></script>
		
		<!-- Bootstrap -->
	    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll -->
		<script src='/assets/js/jquery.slimscroll.min.js'></script>
		
		<!-- Popup Overlay -->
		<script src='/assets/js/jquery.popupoverlay.min.js'></script>

		<!-- Modernizr -->
		<script src='/assets/js/modernizr.min.js'></script>
		
		<!-- Simplify -->
		<script src="/assets/js/simplify/simplify.js"></script>
	
  	</body>
</html>
<!-- Software Developer: JOSHUA M. FRADEJAS -->