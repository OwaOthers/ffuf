@extends('layouts.master')
@section('main-body')
<h3 class="header-text m-top-lg"><i class="fa fa-book"> Books Maintenance</i></h3>
<div class="row">
	<div class="col-lg-12 animated flash">
	    <?php if (session('is_success')): ?>
	        <div class="alert alert-success alert-dismissible fade in" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	        </button>
	            <center><h4 style="color:white">Book was successfully added!<i class="fa fa-check"></i></h4></center>                
	        </div>
	    <?php endif;?>
	    <?php if (session('is_update')): ?>
	        <div class="alert alert-success alert-dismissible fade in" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	        </button>
	            <center><h4 style="color:white">Book was successfully updated!<i class="fa fa-check"></i></h4></center>                
	        </div>
	    <?php endif;?>
	</div>
</div>
<div class="smart-widget widget-green">
	<div class="smart-widget-header">
		<i class="fa fa-book"> Books Table</i>	
		{!! Html::decode(link_to_Route('books.create', '<i class="fa fa-plus"></i> New Book', [], ['class' => 'btn btn-default btn-xs pull-right'])) !!}	    	
	</div>	
	<div class="smart-widget-inner">		
		<div class="smart-widget-body">
			<table class="table table-hover table-striped" id="dataTable">							
	      		<thead>
	        		<tr>	          	
	        			<th>#</th>
	        			<th style="text-align: center;">Title</th>		          				
	        			<th style="text-align: center;">Author</th>		          				
	        			<th style="text-align: center;">Genre</th>		          				
	        			<th style="text-align: center;">Section</th>
	        			<th style="text-align: center;">Status</th>
	        			<th style="text-align: center;">Borrowed By</th>
	          			<th style="text-align: center;">Action</th>
	        		</tr>
	      		</thead>
	      		<tbody>
					@foreach($books as $book)									    	        	
		        	<tr>		          		      		        			           			       				          		          		   
		          		<td>{!! $book->id !!}</td>		          				          		          		   	
		          		<td style="text-align: center;">{!! $book->title !!}</td>			          		
		          		<td style="text-align: center;">{!! $book->author !!}</td>			          		
		          		<td style="text-align: center;">{!! $book->genre->genre !!}</td>			          		
		          		<td style="text-align: center;">{!! $book->section->section !!}</td>			          				          		
		          		<td style="text-align: center;">
		          			@if($book->status < 1)
		          				Available
		          			@else
		          				Borrowed
		          			@endif
		          		</td>
		          		<td style="text-align: center;">
		          			@if($book->status < 1)
		          				<i class="fa fa-times"></i>
		          			@else
		          				{!! $book->borrowed_by !!}
		          			@endif
		          		</td>
		          		<td style="text-align: center;">{!! Html::decode(link_to_Route('books.edit','<i class="fa fa-pencil"></i> Edit', $book->id, array('class' => 'btn btn-info btn-xs')))!!}</td>
		        	</tr>
		        	@endforeach		        	
	      		</tbody>
	    	</table>
		</div>
	</div>
</div>
@stop