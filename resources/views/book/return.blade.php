@extends('layouts.master')
@section('main-body')
<h3 class="header-text m-top-lg">Returning of Book</h3>
<div class="row">
	<div class="col-md-12 animated flash">
	    @if(count($errors) > 0)
	        <div class="alert alert-danger alert-dismissible flash" role="alert">            
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	    </button>
	            <center><h4>Oh snap! You got an error!</h4></center>   
	        </div>
	    @endif
	    <?php if (session('is_success')): ?>
	        <div class="alert alert-success alert-dismissible fade in" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	        </button>
	            <center><h4 style="color:white">Book was successfully returned!<i class="fa fa-check"></i></h4></center>                
	        </div>
	    <?php endif;?>	  
	    <?php if (session('is_error')): ?>
	        <div class="alert alert-danger alert-dismissible fade in" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	        </button>
	            <center><h4 style="color:white">Please select a book to be return!</h4></center>                
	        </div>
	    <?php endif;?>	    
	</div>    
</div>
@if(count($books) > 0)	
	<div class="col-lg-12">
		<div class="smart-widget widget-green">
			<div class="smart-widget-header">
				<i class="fa fa-book"> Lists of Books</i>				
			</div>			
			{!! Form::open(array('route'=>'books.borrow_store','method'=>'POST','files'=>true)) !!}							
			<div class="smart-widget-inner">		
				<div class="smart-widget-body">				
					<div class="row-1">									
						<table class="table table-hover table-striped" id="dataTable">	
			      		<thead>
			        		<tr>	          	
			        			<th>#</th>
			        			<th style="text-align: center;">Title</th>		          				
			        			<th style="text-align: center;">Author</th>		          				
			        			<th style="text-align: center;">Genre</th>		          				
			        			<th style="text-align: center;">Section</th>		        					          		
			        		</tr>
			      		</thead>
			      		<tbody>
							@foreach($books as $book)									    	        	
				        	<tr>		          		      		        			           			       				          		          		   
				          		<td><input type="checkbox" name="book_id[]" value="{!! $book->id !!}"></td>		          				          		          		   	
				          		<td style="text-align: center;">{!! $book->title !!}</td>			          		
				          		<td style="text-align: center;">{!! $book->author !!}</td>			          		
				          		<td style="text-align: center;">{!! $book->genre->genre !!}</td>			          		
				          		<td style="text-align: center;">{!! $book->section->section !!}</td>			          				          					          					          	
				        	</tr>
				        	@endforeach		        	
				      		</tbody>
				    	</table>
			    	</div>			    	
			    	<div class="row-1" style="margin-top: 30px;">
			    		 <div class="form-group">
			    		 	{!! Form::hidden('type','1') !!}							
		                    {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success')) !!}
						</div>	
			    	</div>
				</div>
			</div>
			{!! Form::close() !!}		
		</div>
	</div>
@else
	...all books are available right now
@endif
@stop