@extends('layouts.master')
@section('main-body')
<h3 class="header-text m-top-lg">Adding of Book</h3>
<div class="row">
	<div class="col-md-12 animated flash">
	    @if(count($errors) > 0)
	        <div class="alert alert-danger alert-dismissible flash" role="alert">            
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	    </button>
	            <center><h4>Oh snap! You got an error!</h4></center>   
	        </div>
	    @endif
	</div>    
</div>
<div class="col-lg-6">
	<div class="smart-widget widget-green">	
		<div class="smart-widget-header">
			Book Form							
		</div>
		<div class="smart-widget-inner">		
			<div class="smart-widget-body">
				{!! Form::open(array('route'=>'books.store','method'=>'POST','files'=>true)) !!}							
					<div class="form-group">
						{!! Form::label('title','Title') !!}
						{!! Form::text('title','',['class'=>'form-control','placeholder'=>'Enter Title','required'=>'required']) !!}
						@if($errors->has('title')) <p class="help-block" style="color:red;">{{ $errors->first('title') }}</p> @endif      
					</div>		
					<div class="form-group">
						{!! Form::label('author','Author') !!}
						{!! Form::text('author','',['class'=>'form-control','placeholder'=>'Enter Author','required'=>'required']) !!}
						@if($errors->has('author')) <p class="help-block" style="color:red;">{{ $errors->first('author') }}</p> @endif      
					</div>						
					<div class="form-group">
						{!! Form::label('genre_id','Genre') !!}
						{!! Form::select('genre_id',$genres,'',['class'=>'form-control','required'=>'required']) !!}
						@if($errors->has('genre_id')) <p class="help-block" style="color:red;">{{ $errors->first('genre_id') }}</p> @endif      
					</div>			
					<div class="form-group">
						{!! Form::label('section_id','Section') !!}
						{!! Form::select('section_id',$sections,'',['class'=>'form-control','required'=>'required']) !!}
						@if($errors->has('section_id')) <p class="help-block" style="color:red;">{{ $errors->first('section_id') }}</p> @endif      
					</div>			
					<div class="form-group">
						{!! Html::decode(link_to_Route('books.index', '<i class="fa fa-arrow-left"></i> Cancel', [], ['class' => 'btn btn-default'])) !!}
	                    {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success')) !!}
					</div>	
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop