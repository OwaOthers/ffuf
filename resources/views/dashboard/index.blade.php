@extends('layouts.master')
@section('main-body')
<div class="row m-top-md">	
	<div class="col-lg-4 col-sm-6">
		<div class="statistic-box bg-success m-bottom-md">
			<div class="statistic-title">
				Total Books
			</div>
			<div class="statistic-value">
				{!!  number_format($books_cnt) !!}
			</div>			
			<div class="statistic-icon-background">
				<i class="fa fa-book"></i>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-sm-6">
		<div class="statistic-box bg-success m-bottom-md">
			<div class="statistic-title">
				Available / Returned
			</div>

			<div class="statistic-value">
				{!!  number_format($available) !!}
			</div>
			<div class="statistic-icon-background">
				<i class="fa fa-exchange"></i>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-sm-6">
		<div class="statistic-box bg-success m-bottom-md">
			<div class="statistic-title">
				Borrowed
			</div>

			<div class="statistic-value">
				{!!  number_format($borrowed) !!}
			</div>
			<div class="statistic-icon-background">
				<i class="fa fa-file"></i>
			</div>
		</div>
	</div>
</div>
@stop