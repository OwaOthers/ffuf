$('#category_id').on("change", function(){

    var x = $('#category_id').val();    

    $.ajax({
        type:"POST",
        data: {category_id: x},
        url: "../merchants/sub_category_filter",
        success: function(data){
            $('select#sub_category_id').empty();
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#sub_category_id'));
            });            
        }
    });
});    
