<?php

use Illuminate\Database\Seeder;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	\DB::table('sections')->truncate();
    	\DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		DB::statement("INSERT INTO sections (id, section) VALUES
			(1, 'Circulation'),
			(2, 'Periodical Section'),
			(3, 'General Reference'),
			(4, 'Childerens Section'),
			(5, 'Fiction');");
    }
}
