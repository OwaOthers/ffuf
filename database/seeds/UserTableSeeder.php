<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	\DB::table('users')->truncate();
    	\DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    	$pass = \Hash::make('password');

    	DB::statement("INSERT INTO users (id, name, password) VALUES
			(1, 'admin','$pass');");
    }
}
