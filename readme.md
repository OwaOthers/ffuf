1.) Clone the repository and proceed to the directory of project

2.) Run composer install

3.) Edit .env file
	Set up your database username and password.
	example: 
		DB_USERNAME=root
		DB_PASSWORD=

4.) Create database with schema name: ffuf

5.) Open your git bash

6.) proceed to the directory of the project

7.) run this command: php artisan migrate

8.) run this command: php artisan db:seed

9.) run this command: php artisan serve

10.) run to your browser: localhost:8000


username = admin
password = password

