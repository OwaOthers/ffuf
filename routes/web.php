	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'guest'], function () {
	Route::get('/', 'Auth\AuthController@getLogin');
	Route::get('/login', ['as'=>'login', 'uses'=>'Auth\AuthController@getLogin']);
	Route::post('login', 'Auth\AuthController@postLogin');		
});


Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::group(['middleware' => 'auth'], function () {

	Route::resource('dashboard','DashboardController');
	Route::resource('genres','GenreController');
	Route::resource('sections','SectionController');
	Route::resource('books','BookController');	
	Route::get('borrow_books',['as'=>'books.borrow','uses'=>'BookController@borrow']);
	Route::get('return_books',['as'=>'books.return','uses'=>'BookController@return_book']);
	Route::post('borrow_books/store',['as'=>'books.borrow_store','uses'=>'BookController@borrow_store']);
});